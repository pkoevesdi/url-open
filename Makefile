# SPDX-FileCopyrightText: 2021 Daniel Aleksandersen <https://www.daniel.priv.no/>
# SPDX-License-Identifier: CC0-1.0

PREFIX ?= /usr/local
METADIR = $(PREFIX)/share/metainfo
DESKTOPDIR = $(PREFIX)/share/applications

all: build/url-open.desktop

build/url-open.desktop: src/url-open.desktop
	-mkdir -p $(@D)
	sed -e 's:Exec=url-open:Exec=$(BINDIR)/url-open:' $< > $@

install: build/url-open.desktop src/no.priv.daniel.urlopen.metainfo.xml
	install -D -m 0644 src/url-open.desktop $(DESKTOPDIR)/url-open.desktop
	install -D -m 0644 src/no.priv.daniel.urlopen.metainfo.xml $(METADIR)/no.priv.daniel.urlopen.metainfo.xml

checks: build/url-open.desktop
	desktop-file-validate build/url-open.desktop
	appstreamcli validate src/no.priv.daniel.urlopen.metainfo.xml
	reuse lint

clean:
	rm -rf build/
