<!--- 
SPDX-FileCopyrightText: 2021 Daniel Aleksandersen <https://www.daniel.priv.no/>
SPDX-License-Identifier: CC0-1.0
-->

`url-open` is a simple a program for FreeDesktop environments (Linux, FreeBSD) for opening `.URL` files
([Internet Shortcut files](https://docs.microsoft.com/en-us/windows/win32/lwef/internet-shortcuts)).
The program parses opened `.URL` files, opens their link in your default web browser, and immidiately exits.
It installs purely as a desktop file. All you need to do is to double-click your `.URL` files to open.

Links will open in your default web browser using `xdg-open`.

# Installation instructions

1. Download and extract [current tarball](../../../archive/main.tar.gz)
1. `sudo make install`

or just

1. download the file [`src/url-open.desktop`](../../../raw/branch/main/src/url-open.desktop) 
1. move it to Your usual `.desktop` file location (`/usr/share/applications` on Ubuntu, maybe `/usr/local/share/applications` on other Distros

